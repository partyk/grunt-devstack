# Changelog
All notable changes this project will be documented in this file.

## [Unreleased]

## [1.0.4] - 2017-10-21
### Added
- install like of NPM package
- file CHANGELOG.md
- file LICENSE
- fix bug: missing package imagemin-pngquant
- fix bug: run task when create file in new subdirectory 

## [1.0.3] - 2017-07-21
### Added
- module grunt-pagespeed Grunt plugin to run Google PageSpeed Insights as part of CI
- fix to tmp directory add to git ignore

## [1.0.2] - 2017-07-18
### Added
- module grunt-contrib-imagemin for better optimalise images JPG, GIF, PNG, SVG
- module grunt-svg-to-png for convert fallback from svg to png

### Removed
- module grunt-image

## [1.0.1] - 2017-07-10
### Added
- documentation readme.md
- module grunt-svgmin for minify svg
- module grunt-webp new image format webp from Google

## [1.0.0] - 2017-06-20
### Added
- Grunt DevStack
- module grunt-contrib-watch
- module grunt-contrib-concat
- module grunt-contrib-less
- module grunt-postcss
- postcss processor postcss-discard-duplicates
- postcss processor pixrem
- postcss processor autoprefixer
- postcss processor postcss-will-change
- postcss processor postcss-color-rgba-fallback
- postcss processor postcss-opacity
- postcss processor postcss-pseudoelements
- postcss processor postcss-vmin
- postcss processor cssnano
- module grunt-babel
- module grunt-contrib-clean
- module grunt-image
- module grunt-contrib-uglify