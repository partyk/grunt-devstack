'use strict';

module.exports = function (grunt) {
    var options = {
        url: {
            production: "", //url adresa projektu
            devel: "" //url lokalni
        },
        //cesty
        path: {
            cwd : 'ui', //zdrojovy adresar/current working directory
            dist : 'www/ui',
            dest : 'www/ui', //vystup /destination
            temp: "tmp/grunt", //temp
            bower: "bower_components", //bover
            npm: "node_modules" //npm
        },
        //podpora prohlizecu pro pluginy s fallback kompilaci. Dokumentace -> https://github.com/ai/browserslist
        browsers: ['> 2% in CZ', 'last 3 version', 'ios 6', 'ie 9'],

        //time-grunt bude ukecanejsi
        jitGrunt: true,
    
        // Externi konfig
        pkg: grunt.file.readJSON('package.json'),

        //bower concat package
        bowerPkg : {
            js : {
                dependencies : [
                    '<%= path.bower %>/jquery/dist/jquery.js',
                    '<%= path.bower %>/jquery-validation/dist/jquery.validate.js'
                ]
            }
        }
    };

    //otestuji jestli existuje lokalni config
    if (grunt.file.exists('grunt-config-local.json')) {
        Object.assign(options, grunt.file.readJSON('grunt-config-local.json'))
    }

    grunt.config.init(options);

    /**
	 * modul 'time-grunt' : pro debug. Zobrazi delku jednotlivych procesu
     * git https://github.com/sindresorhus/time-grunt 
	 */
    require('time-grunt')(grunt);
    
    require('load-grunt-config')(grunt, { config: options }); 
};