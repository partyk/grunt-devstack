/**
 * modul 'grunt-webp' : generuje novy format pro chrome *.webp
 * git https://github.com/somerandomdude/grunt-webp
 */

'use strict';

module.exports = {
     /**
     * nastaveni pro vsechny tasky
     */
    options: {
        binpath: require('webp-bin').path,
        preset: 'photo',
        verbose: true,
        quality: 90,
        alphaQuality: 80,
        compressionMethod: 6,
        segments: 4,
        psnr: 42,
        sns: 50,
        filterStrength: 40,
        filterSharpness: 3,
        simpleFilter: true,
        partitionLimit: 50,
        analysisPass: 6,
        multiThreading: true,
        lowMemory: false,
        alphaMethod: 0,
        alphaFilter: 'best',
        alphaCleanup: true,
        noAlpha: false,
        lossless: false
    },

    /**
     * prevod image do formatu WebP pro chrome z '<%= path.temp %>/image/' do '<%= path.dest %>/image/'
     */
    tempImage: {
        files: [{
            expand: true,
            cwd: '<%= path.temp %>/image/',  //zdrojovy adresar
            src: ['**/*.{png,jpg,gif}'], //nastavim aby se koptimalizovali jen obrazky
            dest: '<%= path.dest %>/image/'  //vystupni adresar
        }]
    },

    /**
     * prevod image do formatu WebP pro chrome z <%= path.cwd %>/image/ do <%= path.dest %>/image/
     */
    default: {
        files: [{
            expand: true,
            cwd: '<%= path.cwd %>/image/',  //zdrojovy adresar
            src: ['**/*.{png,jpg,gif}'], //nastavim aby se koptimalizovali jen obrazky
            dest: '<%= path.dest %>/image/'  //vystupni adresar
        }]
    } 

}; // module.exports