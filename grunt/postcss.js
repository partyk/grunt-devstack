/**
 * modul 'grunt-postcss' : vice ucelovy modul, ktery umi za pomoci processors pridat do CSS fallback, prefixery, rem/px, atd..
 * git https://github.com/nDmitry/grunt-postcss
 */

'use strict';

module.exports = (grunt, options) => {

  let configModule = {
    options: {
      map: true,
      /**
       * info o modulech https://webdesign.tutsplus.com/tutorials/using-postcss-for-cross-browser-compatibility--cms-24567
       */
      processors: [
        /*
        require("postcss-easy-import")({
          plugins: [
            // manual https://stylelint.io/user-guide/postcss-plugin/
            require("stylelint", {
              configFile: '.stylelintrc'
            }) // Kontrola jeste pred upravami
          ]
        }),
        */
        require('postcss-discard-duplicates')(),
        require('pixrem')({ rootValue: 10 }), // rem -> px fallback, defaultni hodnota pro vypocet je 10px
        require('autoprefixer')({ browsers: options.browsers }), // pridani prefixu
        require('postcss-will-change')(),
        require('postcss-color-rgba-fallback')(),
        require('postcss-opacity')(),
        require('postcss-pseudoelements')(),
        require('postcss-vmin')(),
        require('cssnano')()
      ]
    },
    default: {
      src: '<%= path.dest %>/css/**/*.css'
    }
  };

  return configModule;

}; // module.exports