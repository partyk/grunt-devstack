/**
 * modul 'grunt-svg-to-png' : optimalizace obrazku
 * git https://github.com/1000ch/grunt-image
 */

'use strict';

module.exports = {
     /**
     * nastaveni pro vsechny tasky
     */
    options: {

    },
    /**
     * defaultni nastaveni vytvori PNG ze SVG do tempu
     */
    default: {
        files: [{
            //expand: true,
            cwd: '<%= path.cwd %>/image/',  //zdrojovy adresar
            src: ['**/*.svg'], //nastavim aby se koptimalizovali jen obrazky
            dest: '<%= path.temp %>/image/'  //vystupni adresar
        }]
    } 

}; // module.exports