/**
 * modul 'grunt-contrib-concat' : spojovani souboru do jednoho
 * git https://github.com/gruntjs/grunt-contrib-concat
 */

'use strict';

module.exports = (grunt, options) => {

  let configModule = {

    options: {
      stripBanners: true, //vyhozeni komentaru v balicku,
      nonull: true //jen nenulove soubory,
    },

    dependencies: {
      src: [options.bowerPkg.js.dependencies],
      dest: '<%= path.dest %>/js/dist/dependencies.js',  
      nonull: true   
    }
      
  };

  return configModule;

}; // module.exports