/** 
 * modul 'grunt-contrib-watch' : sleduje zmeny a na zaklade toho spousti procesy
 * git https://github.com/gruntjs/grunt-contrib-watch
 */

'use strict';

module.exports = {
    options: {
      spawn: false,
      reload: true
    },
    /**
     * watch less
     */
    less: {
        filter: 'isFile',
        files: ['<%= path.cwd %>/css/src/**/*.less'],
        tasks: ['less']
    },
    /**
     * watch babel
     */
    babel: {
        filter: 'isFile',
        files: ['<%= path.cwd %>/js/es6/src/**/*.js'],
        tasks: ['babel:devel']
    }
}; // module.exports