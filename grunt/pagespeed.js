/**
 * rychlý test stránky přes pagespeed od google
 * git: https://github.com/jrcryer/grunt-pagespeed
 */

'use strict';

module.exports = {
    options: {
        nokey: true,
        url: "<%= url.production %>"
    },
    desktop: {
        options: {
            paths: ["/"],
            locale: "cs_CS",
            strategy: "desktop",
            threshold: 70 //minimalni pozadovane score
        }
    },
    mobile: {
        options: {
            paths: ["/"],
            locale: "cs_CS",
            strategy: "mobile",
            threshold: 70 //minimalni pozadovane score
        }
    }
}; // module.exports