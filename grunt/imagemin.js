/**
 * modul 'grunt-contrib-imagemin' : optimalizace obrazku
 * git https://github.com/gruntjs/grunt-contrib-imagemin
 */

'use strict';

const mozjpeg = require('imagemin-mozjpeg');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');

module.exports = {
    /**
    * nastaveni pro vsechny tasky
    */
    options: {
        use: [
            mozjpeg({
                //quality: 80
            }),
            imageminGifsicle({
                optimizationLevel: 3
            }),
            /*imageminJpegtran({
                
            }),*/
            imageminPngquant({
                quality: '60-80'
            }),
            imageminSvgo({
                plugins: [
                    {removeViewBox: false},
                    {removeUselessStrokeAndFill: false}
                ]
            })
        ] // Example plugin usage
    },

    /**
     * optimalizuje image z tempu napr. png fallBackSvg a da je do DEST. Optimalizuje img z '<%= path.temp %>/image/' do '<%= path.dest %>/image/'
     */
    tempImage: {
        files: [{
            expand: true,
            cwd: '<%= path.temp %>/image/',  //zdrojovy adresar
            src: ['**/*.{png,jpg,gif,svg}'], //nastavim aby se optimalizovali jen obrazky
            dest: '<%= path.dest %>/image/'  //vystupni adresar
        }]
    },

    /**
     * defaultni nastaveni. Optimalizuje img z '<%= path.cwd %>/image/' do '<%= path.dest %>/image/'
     */
    default: {
        files: [{
            expand: true,
            cwd: '<%= path.cwd %>/image/',  //zdrojovy adresar
            src: ['**/*.{png,jpg,gif,svg}'], //nastavim aby se koptimalizovali jen obrazky
            dest: '<%= path.dest %>/image/'  //vystupni adresar
        }]
    }

}; // module.exports