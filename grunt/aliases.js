/**
 * konfiguracni soubor pro tasky
 */

'use strict';

module.exports = {

  //spolecne procesy na spusteni default/build
  runDefault : [
    'clean:tmp', //vycistim tmp
    'clean:css', //vycistim css
    'clean:jsConcat', //vycistim js concat
    'clean:es6', //vycistim es6
    'less', //spustim kompilaci less
    'concat', //spustim concat
  ],

  //fallBack
  imageFallBack : [
    'svg2png', //fallBack svg to png ulozim do temp/image
    'imagemin:tempImage', //vse co je v tempu/image prevedu do www/image a optimalizuji
    'webp:tempImage' //vytvorim webp format 
  ],

  imageDefault : [
   'imagemin:default', //optimalizace image ui/image do www/image
   'webp:default' //vytvorim webp format
  ],

  // grunt defaultni spusteni
  default: [
    'runDefault', //spustim spolecne procesy
    'babel:devel', //spustim devel
    'watch' //spustim watch
  ],

  //produke
  build: [
    'runDefault',
    'postcss',  //spustim postCSS moduly
    'babel:build', //spustim build
    'uglify', //minifikace js
  ],

  //image
  'build-image': [
    'clean:tmpImage',
    'clean:image',
    'imageFallBack', //vytvorim fallBack image
    'imageDefault' //optimalizuji image
  ]

}; // module.exports
