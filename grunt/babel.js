/**
* modul 'grunt-babel' : kompiluje ES6/7 do ES5
* https://github.com/babel/grunt-babel //modul pro grunt
* https://babeljs.io/docs/setup/#installation //o babel
*/

'use strict';

module.exports = {
    options: {
        sourceMap: true, //vytvorim sourceMap
        comments: false, //vyhodim komentare
        presets: ['es2015'] //nastavim ES5
    },
    //build
    build : {
        options: {
            minified: true //minifikace
        },
        expand: true, // Enable dynamic expansion.
        filter: 'isFile',
        cwd: '<%= path.cwd %>/js/es6/src', // cesta k src
        src: ['**/*.js'], // co se ma kompilovat
        dest: '<%= path.dest %>/js/es6', //cesta kam se ma kompilovat
        //ext: '.js', //zmenim koncovku
        //extDot: 'last' //koncovka se meni po prvni tecce proto se umaze .js
    },
    //devel
    devel : {
        expand: true, // Enable dynamic expansion.
        filter: 'isFile',
        cwd: '<%= path.cwd %>/js/es6/src', // cesta k src
        src: ['**/*.js'], // co se ma kompilovat
        dest: '<%= path.dest %>/js/es6', //cesta kam se ma kompilovat
        //ext: '.js', //zmenim koncovku
        //extDot: 'last' //koncovka se meni po prvni tecce proto se umaze .js
    }

}; // module.exports