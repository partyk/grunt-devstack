/**
 * modul 'grunt-contrib-clean' : pro mazani nepotrebnych souboru/adresaru
 * git https://github.com/gruntjs/grunt-contrib-clean
 */

'use strict';

module.exports = (grunt, options) => {

    let configModule = {
        all : ['<%= path.dest %>', '<%= path.temp %>'],
        tmp : ['<%= path.temp %>'],
        tmpImage : ['<%= path.temp %>/image'],
        css: ['<%= path.dest %>/css'],
        jsConcat: ['<%= path.dest %>/js/dist'],
        es6: ['<%= path.dest %>/js/es6'],
        image: ['<%= path.dest %>/image']        
    }

    return configModule;

}; // module.exports