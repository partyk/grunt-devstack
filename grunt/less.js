/**
 * modul 'grunt-contrib-less' : kompiluje less->css
 * git https://github.com/gruntjs/grunt-contrib-less
 */

'use strict';

module.exports = {
    /**
     * nastaveni pro vsechny tasky
     */
    options: {
        sourceMap: true, // source map
        //sourceMapURL: 'ui/css/',
        sourceMapRootpath: '/'
    },

    /**
     * defaultni kompilace less. Kompiluje vse z adresare '<%= path.cwd %>/css/src/' do '<%= path.dest %>/css/'
     */
    default: {
        files: [{
            expand: true,
            cwd: '<%= path.cwd %>/css/src/',  //zdrojovy adresar
            src: ['**/*.less'], //nastavim aby se kompilovali soubory s koncovkou *.less vcetne podadresaru
            dest: '<%= path.dest %>/css/',  //vystupni adresar
            ext: '.css'
        }]
    }

}; // module.exports