# Grunt DevStack

Grunt Dev Stack with less, bable, concat, imagemin, svg2png, uglify, webp, postcss and more.

---

## Instalation from GitLab

1.  Download or clone repository
2.  run ```npm install```
3.  run ```bower install```

## Instalation from NPM

1. npm install grunt-devstack
2.  run ```npm install```
3.  run ```bower install```

## Grunt tasks

1. ```grunt``` – for development.
2. ```grunt build``` – for production.
3. ```grunt build-image``` – for optimize images.

## Resources

-   [NPM](https://www.npmjs.com/)
-   [Grunt](http://gruntjs.com)
-   [Bower](https://bower.io/)

## Change log

[CHANGELOG.md](CHANGELOG.md)